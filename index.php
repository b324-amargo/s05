
<!DOCTYPE html>
<html>
<head>
	<title>S05: Client-Server Communication (Discussion 2)</title>
</head>
<body>
	<?php session_start(); ?>

	<?php if(!isset($_SESSION['login'])){; ?>
		<h1>Login User here:</h1>
		<div>
			<form method="POST" action="./server.php" style="display: inline-block">
		        <input type="hidden" name="action" value="login"/>

		        Email: 
		        <input type="text" name="email" required/>

		        Password: 
		        <input type="text" name="password" required/>

		        <button type="submit">Login</button>
		    </form>
		</div>
	<?php } ?>

	<?php if(isset($_SESSION['login'])){; ?>
		<div>
			<form method="POST" action="./server.php">
				<p>Hello, <?php echo $_SESSION['login'][0]->username;?></p>
				<input type="hidden" name="action" value="logout"/>
				<button type="submit">Logout</button>
			</form>
		</div>
	<?php }; ?>
	
</body>
</html>